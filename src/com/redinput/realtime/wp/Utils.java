package com.redinput.realtime.wp;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.http.AndroidHttpClient;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import com.redinput.realtime.wp.download.AlarmReceiver;
import com.redinput.realtime.wp.download.BootReceiver;
import com.redinput.realtime.wp.download.DetectChanged;

public class Utils {

	public static void enableOnBoot(Context context) {
		ComponentName receiver = new ComponentName(context, BootReceiver.class);
		PackageManager pm = context.getPackageManager();
		pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
	}

	public static void disableOnBoot(Context context) {
		ComponentName receiver = new ComponentName(context, BootReceiver.class);
		PackageManager pm = context.getPackageManager();
		pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

	public static void enableDetection(Context context) {
		ComponentName receiver = new ComponentName(context, DetectChanged.class);
		PackageManager pm = context.getPackageManager();
		pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
	}

	public static void disableDetection(Context context) {
		ComponentName receiver = new ComponentName(context, DetectChanged.class);
		PackageManager pm = context.getPackageManager();
		pm.setComponentEnabledSetting(receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

	public static void enableRepeating(Context context, long interval) {
		AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, AlarmReceiver.class);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

		alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime() + interval, interval, alarmIntent);
		context.sendBroadcast(intent);
	}

	public static void disableRepeating(Context context) {
		AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		Intent intent = new Intent(context, AlarmReceiver.class);
		PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

		alarmMgr.cancel(alarmIntent);
	}

	public static boolean shouldDownload(Context context) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
		boolean onlyWifi = sharedPref.getBoolean("only_wifi", false);

		if (isConnectionAvailable(context)) {
			if (onlyWifi) {
				if (isWifiConnected(context)) {
					return true;
				}
			} else {
				return true;
			}
		}
		return false;

	}

	private static boolean isConnectionAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
		return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
	}

	private static boolean isWifiConnected(Context context) {
		ConnectivityManager connManager = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		if (mWifi.isConnected()) {
			return true;
		} else {
			return false;
		}
	}

	public static Bitmap decodeSampledBitmapFromStream(String urlImage, int reqWidth, int reqHeight)
			throws URISyntaxException, IOException {

		AndroidHttpClient client = AndroidHttpClient.newInstance("Realtime WP 2.0/Android");
		HttpGet get = new HttpGet();
		get.setURI(new URI(urlImage));

		HttpResponse response = client.execute(get);
		InputStream is = response.getEntity().getContent();
		BufferedInputStream bis = new BufferedInputStream(is);

		final BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true;
		Rect rect = new Rect(-1, -1, -1, -1);
		BitmapFactory.decodeStream(bis, rect, options);

		options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
		options.inJustDecodeBounds = false;

		response = client.execute(get);
		is = response.getEntity().getContent();
		bis = new BufferedInputStream(is);

		Bitmap bmp = BitmapFactory.decodeStream(bis, rect, options);

		bis.close();
		is.close();
		client.close();

		return bmp;
	}

	private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth,
			int reqHeight) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > reqHeight || width > reqWidth) {

			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			while ((halfHeight / inSampleSize) > reqHeight
					&& (halfWidth / inSampleSize) > reqWidth) {
				inSampleSize *= 2;
			}
		}

		return inSampleSize;
	}
}
