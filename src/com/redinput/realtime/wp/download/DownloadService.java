package com.redinput.realtime.wp.download;

import java.io.IOException;
import java.net.URISyntaxException;

import android.app.IntentService;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.Rect;
import android.location.Location;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Display;
import android.view.WindowManager;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.redinput.realtime.wp.Utils;

public class DownloadService extends IntentService implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	public DownloadService() {
		super("RealtimeWP download service");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
		String urlImage = sharedPref.getString("url", "");

		Editor editor = sharedPref.edit();
		editor.putBoolean("changing", true);
		editor.apply();

		if (!TextUtils.isEmpty(urlImage)) {
			if (Utils.shouldDownload(this)) {
				try {
					WindowManager wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
					Display display = wm.getDefaultDisplay();
					Point size = new Point();
					display.getSize(size);
					int width = size.x;
					int height = size.y;

					if (width > height) {
						int temp = width;
						width = height;
						height = temp;
					}

					Bitmap origWallpaper = Utils.decodeSampledBitmapFromStream(urlImage, width * 2,
							height);

					Bitmap wallpaper;
					if (sharedPref.getBoolean("adjust", false)) {
						wallpaper = Bitmap.createScaledBitmap(origWallpaper, width * 2, height,
								true);
						origWallpaper.recycle();

					} else {
						wallpaper = Bitmap.createScaledBitmap(origWallpaper, height, height, true);
						origWallpaper.recycle();
					}

					if (sharedPref.getBoolean("locate", false)) {
						LocationClient mLocationClient = new LocationClient(this, this, this);
						mLocationClient.connect();

						while (mLocationClient.isConnecting()) {
							Thread.sleep(500);
						}

						if (mLocationClient.isConnected()) {
							Location mCurrentLocation = mLocationClient.getLastLocation();

							if (mCurrentLocation != null) {
								int longitude = (int) mCurrentLocation.getLongitude();
								int displace = calculateDisplaceCenter(width * 2, longitude);

								Rect r = null;
								if (displace > 0) {
									r = new Rect(0, 0, displace, wallpaper.getHeight());
									Bitmap bmpR = cropBitmap(wallpaper, r);

									r = new Rect(displace, 0, wallpaper.getWidth(),
											wallpaper.getHeight());
									Bitmap bmpL = cropBitmap(wallpaper, r);

									wallpaper = combineImages(bmpL, bmpR);
									bmpR.recycle();
									bmpL.recycle();

								} else if (displace < 0) {
									displace = -displace;

									r = new Rect(0, 0, wallpaper.getWidth() - displace,
											wallpaper.getHeight());
									Bitmap bmpR = cropBitmap(wallpaper, r);

									r = new Rect(wallpaper.getWidth() - displace, 0,
											wallpaper.getWidth(), wallpaper.getHeight());
									Bitmap bmpL = cropBitmap(wallpaper, r);

									wallpaper = combineImages(bmpL, bmpR);
									bmpR.recycle();
									bmpL.recycle();
								}
							}

							mLocationClient.disconnect();
						}
					}

					WallpaperManager wManager = WallpaperManager.getInstance(this);
					wManager.setBitmap(wallpaper);

				} catch (IOException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (URISyntaxException e) {
					e.printStackTrace();
				}
			}
		}

		stopSelf();
	}

	private int calculateDisplaceCenter(int width, int longitude) {
		return longitude * width / 360;
	}

	public static Bitmap cropBitmap(Bitmap bitmap, Rect rect) {
		int w = rect.right - rect.left;
		int h = rect.bottom - rect.top;
		Bitmap ret = Bitmap.createBitmap(w, h, bitmap.getConfig());
		Canvas canvas = new Canvas(ret);
		canvas.drawBitmap(bitmap, -rect.left, -rect.top, null);
		return ret;
	}

	public Bitmap combineImages(Bitmap c, Bitmap s) {
		Bitmap cs = null;

		int width = c.getWidth() + s.getWidth();
		int height = c.getHeight();

		cs = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

		Canvas comboImage = new Canvas(cs);

		comboImage.drawBitmap(c, 0f, 0f, null);
		comboImage.drawBitmap(s, c.getWidth(), 0f, null);

		return cs;
	}

	@Override
	public void onConnectionFailed(ConnectionResult connectionResult) {
	}

	@Override
	public void onConnected(Bundle dataBundle) {
	}

	@Override
	public void onDisconnected() {
	}

}
