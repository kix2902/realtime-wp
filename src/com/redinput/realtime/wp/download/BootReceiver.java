package com.redinput.realtime.wp.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.redinput.realtime.wp.Utils;

public class BootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			long interval = Long.valueOf(sharedPref.getString("updates_interval", "3600000"));

			Utils.enableRepeating(context, interval);
		}
	}

}
