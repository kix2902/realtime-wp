package com.redinput.realtime.wp.download;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.redinput.realtime.wp.R;
import com.redinput.realtime.wp.Utils;

public class DetectChanged extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.intent.action.WALLPAPER_CHANGED")) {
			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

			if (!sharedPref.getBoolean("changing", false)) {
				Editor editor = sharedPref.edit();
				editor.remove("locate");
				editor.remove("adjust");
				editor.remove("url");
				editor.apply();

				Utils.disableRepeating(context);
				Utils.disableOnBoot(context);
				Utils.disableDetection(context);

				Toast.makeText(context, R.string.auto_disable, Toast.LENGTH_SHORT).show();

			} else {
				Editor editor = sharedPref.edit();
				editor.remove("changing");
				editor.apply();
			}
		}
	}
}
