package com.redinput.realtime.wp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnItemClickListener {

	private ArrayList<Item> items;
	private GridView gridView;
	private ItemsAdapter adapter;

	private SharedPreferences sharedPref;

	private MenuItem itemDisable;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		items = new ArrayList<Item>();

		items.add(new Item(getString(R.string.earth_title), getString(R.string.earth_description),
				getString(R.string.earth_credits), getResources().getDrawable(R.drawable.earth)));
		items.add(new Item(getString(R.string.moon_title), getString(R.string.moon_description),
				getString(R.string.moon_credits), getResources().getDrawable(R.drawable.moon)));
		items.add(new Item(getString(R.string.sun_title), getString(R.string.sun_description),
				getString(R.string.sun_credits), getResources().getDrawable(R.drawable.sun)));

		gridView = (GridView) findViewById(R.id.gridImages);
		gridView.setOnItemClickListener(this);

		adapter = new ItemsAdapter(this, items);
		gridView.setAdapter(adapter);

		sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_main, menu);

		itemDisable = menu.findItem(R.id.menuDisable);

		if (TextUtils.isEmpty(sharedPref.getString("url", ""))) {
			itemDisable.setVisible(false);
		} else {
			itemDisable.setVisible(true);
		}

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.menuSettings:
				Intent iSettings = new Intent(this, SettingsActivity.class);
				startActivity(iSettings);

				return true;

			case R.id.menuDisable:
				Editor editor = sharedPref.edit();
				editor.remove("locate");
				editor.remove("adjust");
				editor.remove("url");
				editor.apply();

				itemDisable.setVisible(false);

				Utils.disableRepeating(this);
				Utils.disableOnBoot(this);
				Utils.disableDetection(this);
				resetWallpaper();

				Toast.makeText(this, R.string.restoring, Toast.LENGTH_SHORT).show();
				finish();

				return true;

			default:
				return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
		String name = "";
		int subarray = -1;
		int select = -1;
		switch (position) {
			case 0:
				name = getString(R.string.earth_title);
				select = R.string.earth_select;
				subarray = R.array.earth_options;
				break;

			case 1:
				name = getString(R.string.moon_title);
				select = R.string.dialog_message;
				break;

			case 2:
				name = getString(R.string.sun_title);
				select = R.string.sun_select;
				subarray = R.array.sun_options;
				break;

		}
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		if ((position == 0) || (position == 2)) {
			builder.setTitle(name + ": " + getString(select));
			builder.setItems(subarray, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					applyWallpaper(position, which);
					dialog.dismiss();
				}
			});

		} else if (position == 1) {
			builder.setTitle(name);
			builder.setMessage(select);
			builder.setPositiveButton(R.string.dialog_ok, new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					applyWallpaper(position, 0);
					dialog.dismiss();
				}
			});
		}

		builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		builder.show();
	}

	private void applyWallpaper(int position, int suboption) {
		Editor editor = sharedPref.edit();

		switch (position) {
			case 0:
				editor.putBoolean("locate", true);
				editor.putBoolean("adjust", true);

				switch (suboption) {
					case 0:
						editor.putString("url", "http://static.die.net/earth/mercator/1600.jpg");
						break;

					case 1:
						editor.putString("url", "http://static.die.net/earth/peters/1600.jpg");
						break;

					case 2:
						editor.putString("url", "http://static.die.net/earth/rectangular/1600.jpg");
						break;
				}

				break;

			case 1:
				editor.putBoolean("locate", false);
				editor.putBoolean("adjust", false);
				editor.putString("url", "http://static.die.net/moon/512.jpg");

				break;

			case 2:
				editor.putBoolean("locate", false);
				editor.putBoolean("adjust", false);

				switch (suboption) {
					case 0:
						editor.putString("url",
								"http://soho.esac.esa.int/data/realtime/eit_171/1024/latest.jpg");
						break;

					case 1:
						editor.putString("url",
								"http://soho.esac.esa.int/data/realtime/eit_195/1024/latest.jpg");
						break;

					case 2:
						editor.putString("url",
								"http://soho.esac.esa.int/data/realtime/eit_284/1024/latest.jpg");
						break;

					case 3:
						editor.putString("url",
								"http://soho.esac.esa.int/data/realtime/eit_304/1024/latest.jpg");
						break;
				}

				break;
		}

		editor.apply();
		long interval = Long.valueOf(sharedPref.getString("updates_interval", "3600000"));

		Utils.enableDetection(this);
		Utils.enableOnBoot(this);
		Utils.enableRepeating(this, interval);
		saveCurrentWallpaper();

		Toast.makeText(this, R.string.updating, Toast.LENGTH_LONG).show();
		finish();
	}

	private void saveCurrentWallpaper() {
		if (Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
			new File(Environment.getExternalStorageDirectory() + "/.realtime/").mkdirs();
			File imgFile = new File(Environment.getExternalStorageDirectory()
					+ "/.realtime/wallpaper.jpg");

			WallpaperManager wManager = WallpaperManager.getInstance(this);
			Bitmap wallpaper = drawableToBitmap(wManager.getDrawable());

			try {
				FileOutputStream fos = new FileOutputStream(imgFile);
				wallpaper.compress(CompressFormat.JPEG, 100, fos);
				fos.flush();
				fos.close();

			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			}
		}
	}

	public static Bitmap drawableToBitmap(Drawable drawable) {
		if (drawable instanceof BitmapDrawable) {
			return ((BitmapDrawable) drawable).getBitmap();
		}

		Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
		drawable.draw(canvas);

		return bitmap;
	}

	private void resetWallpaper() {
		if (Environment.getExternalStorageState().equalsIgnoreCase(Environment.MEDIA_MOUNTED)) {
			new File(Environment.getExternalStorageDirectory() + "/.realtime/").mkdirs();
			File imgFile = new File(Environment.getExternalStorageDirectory()
					+ "/.realtime/wallpaper.jpg");

			WallpaperManager wManager = WallpaperManager.getInstance(this);
			try {
				wManager.setStream(new FileInputStream(imgFile));

			} catch (FileNotFoundException e) {
			} catch (IOException e) {
			}
		}
	}
}
