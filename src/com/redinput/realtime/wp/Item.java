package com.redinput.realtime.wp;

import android.graphics.drawable.Drawable;

public class Item {
	private String name, description, credits;
	private Drawable image;

	public Item(String name, String description, String credits, Drawable image) {
		this.name = name;
		this.description = description;
		this.credits = credits;
		this.image = image;
	}

	public Item(String name, String description, String credits) {
		this(name, description, credits, null);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCredits() {
		return credits;
	}

	public void setCredits(String credits) {
		this.credits = credits;
	}

	public Drawable getImage() {
		return image;
	}

	public void setImage(Drawable image) {
		this.image = image;
	}

}
