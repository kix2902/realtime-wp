package com.redinput.realtime.wp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.widget.Toast;

public class EasterEggReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if (intent.getAction().equals("android.provider.Telephony.SECRET_CODE")) {
			String numero = intent.getData().getHost();
			if (numero.equals("73258463")) {
				SharedPreferences sharedPref = PreferenceManager
						.getDefaultSharedPreferences(context);
				if (!sharedPref.getBoolean("EasterEggRevealed", false)) {
					Editor edit = sharedPref.edit();
					edit.putBoolean("EasterEggRevealed", true);
					edit.apply();

					Toast.makeText(context, R.string.easter_egg_revealed, Toast.LENGTH_SHORT)
							.show();
				}

				Intent iLaunch = new Intent(context, MainActivity.class);
				iLaunch.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(iLaunch);
			}
		}
	}
}
