package com.redinput.realtime.wp;

import android.content.Intent;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceFragment;

import com.redinput.realtime.wp.billingutil.IabHelper;
import com.redinput.realtime.wp.billingutil.IabResult;

public class SettingsFragment extends PreferenceFragment implements OnPreferenceClickListener {

	private IabHelper mHelper;
	private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAldQ7qvpikg+K/8HOVnmEWNBTX3PGYVrfUCBJZnb6ZK/PmKx6X6BrPIoKsw1AEa3DR4dkvXzqF9AL/zlmitu9jHVYuM8RVP2gvLCcGPeX5GFn9fxKA2sdW11dEN/hFF1Fj17SICv+Izt3+DuQ+vdnQmLV56hlriukAM/XQHOUvq++VZxqZEMOkTExRUEJvm2/Z5s3xcpIwyA4KxQBEkzwrm2GG5WkobWcXhCjVOoBp1yO72nhHQUvvuSpOm5tlHyvhiweACkaXonSCKzd3l7QwPBCPb3YC+IeSWcWi54li9lzbzWSbCbOFcedxDcKWFPj+h2SCG4gBhwhJ6z4P4ZUHQIDAQAB";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.settings);

		mHelper = new IabHelper(getActivity(), base64EncodedPublicKey);

		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isSuccess()) {
					findPreference("donate").setOnPreferenceClickListener(SettingsFragment.this);
				} else {
					getPreferenceScreen().removePreference(findPreference("donate"));
				}
			}
		});
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper == null)
			return;

		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	@Override
	public boolean onPreferenceClick(Preference preference) {
		if (preference.getKey().equalsIgnoreCase("donate")) {
			startActivity(new Intent(getActivity(), DonateActivity.class));
		}

		return false;
	}

}
