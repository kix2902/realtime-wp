package com.redinput.realtime.wp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.redinput.realtime.wp.billingutil.IabHelper;
import com.redinput.realtime.wp.billingutil.IabResult;
import com.redinput.realtime.wp.billingutil.Inventory;
import com.redinput.realtime.wp.billingutil.Purchase;

public class DonateActivity extends Activity {

	private IabHelper mHelper;
	private static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAldQ7qvpikg+K/8HOVnmEWNBTX3PGYVrfUCBJZnb6ZK/PmKx6X6BrPIoKsw1AEa3DR4dkvXzqF9AL/zlmitu9jHVYuM8RVP2gvLCcGPeX5GFn9fxKA2sdW11dEN/hFF1Fj17SICv+Izt3+DuQ+vdnQmLV56hlriukAM/XQHOUvq++VZxqZEMOkTExRUEJvm2/Z5s3xcpIwyA4KxQBEkzwrm2GG5WkobWcXhCjVOoBp1yO72nhHQUvvuSpOm5tlHyvhiweACkaXonSCKzd3l7QwPBCPb3YC+IeSWcWi54li9lzbzWSbCbOFcedxDcKWFPj+h2SCG4gBhwhJ6z4P4ZUHQIDAQAB";

	private Button btn1e, btn2e, btn5e, btn10e, btn20e, btn50e, btn100e;

	private static final String SKU_1E = "realtime_1e";
	private static final String SKU_2E = "realtime_2e";
	private static final String SKU_5E = "realtime_5e";
	private static final String SKU_10E = "realtime_10e";
	private static final String SKU_20E = "realtime_20e";
	private static final String SKU_50E = "realtime_50e";
	private static final String SKU_100E = "realtime_100e";

	private static final int REQUEST_PURCHASE = 2902;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_donate);

		init();

		mHelper = new IabHelper(this, base64EncodedPublicKey);
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			@Override
			public void onIabSetupFinished(IabResult result) {
				if (result.isSuccess()) {
					consumePurchases();
				}
			}
		});
	}

	private void init() {
		btn1e = (Button) findViewById(R.id.btn_1e);
		btn2e = (Button) findViewById(R.id.btn_2e);
		btn5e = (Button) findViewById(R.id.btn_5e);
		btn10e = (Button) findViewById(R.id.btn_10e);
		btn20e = (Button) findViewById(R.id.btn_20e);
		btn50e = (Button) findViewById(R.id.btn_50e);
		btn100e = (Button) findViewById(R.id.btn_100e);

		disableListeners();
	}

	private void enableListeners() {
		btn1e.setOnClickListener(clickPurchaseListener);
		btn2e.setOnClickListener(clickPurchaseListener);
		btn5e.setOnClickListener(clickPurchaseListener);
		btn10e.setOnClickListener(clickPurchaseListener);
		btn20e.setOnClickListener(clickPurchaseListener);
		btn50e.setOnClickListener(clickPurchaseListener);
		btn100e.setOnClickListener(clickPurchaseListener);

		setProgressBarIndeterminateVisibility(false);
	}

	private void disableListeners() {
		btn1e.setOnClickListener(clickWaitListener);
		btn2e.setOnClickListener(clickWaitListener);
		btn5e.setOnClickListener(clickWaitListener);
		btn10e.setOnClickListener(clickWaitListener);
		btn20e.setOnClickListener(clickWaitListener);
		btn50e.setOnClickListener(clickWaitListener);
		btn100e.setOnClickListener(clickWaitListener);

		setProgressBarIndeterminateVisibility(true);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (mHelper == null)
			return;

		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			super.onActivityResult(requestCode, resultCode, data);
		}
	}

	private void consumePurchases() {
		mHelper.queryInventoryAsync(mQueryInventoryFinishedListener);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (mHelper != null) {
			mHelper.dispose();
			mHelper = null;
		}
	}

	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		@Override
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			if (result.isSuccess()) {
				Toast.makeText(DonateActivity.this, R.string.thank_you, Toast.LENGTH_SHORT).show();

				mHelper.consumeAsync(purchase, mOnConsumeFinishedListener);

			} else {
				enableListeners();
			}
		}
	};

	IabHelper.QueryInventoryFinishedListener mQueryInventoryFinishedListener = new IabHelper.QueryInventoryFinishedListener() {
		@Override
		public void onQueryInventoryFinished(IabResult result, Inventory inv) {
			if (inv.hasPurchase(SKU_1E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_1E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_2E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_2E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_5E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_5E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_10E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_10E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_20E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_20E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_50E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_50E), mOnConsumeFinishedListener);

			} else if (inv.hasPurchase(SKU_100E)) {
				mHelper.consumeAsync(inv.getPurchase(SKU_100E), mOnConsumeFinishedListener);

			} else {
				enableListeners();
			}
		}
	};

	IabHelper.OnConsumeFinishedListener mOnConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		@Override
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			enableListeners();
		}
	};

	OnClickListener clickPurchaseListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			switch (v.getId()) {
				case R.id.btn_1e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_1E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_2e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_2E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_5e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_5E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_10e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_10E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_20e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_20E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_50e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_50E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;

				case R.id.btn_100e:
					mHelper.launchPurchaseFlow(DonateActivity.this, SKU_100E, REQUEST_PURCHASE,
							mPurchaseFinishedListener);
					break;
			}
			disableListeners();
		}
	};

	OnClickListener clickWaitListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Toast.makeText(DonateActivity.this, R.string.loading, Toast.LENGTH_SHORT).show();
		}
	};

}
