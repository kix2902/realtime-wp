package com.redinput.realtime.wp;

import java.util.ArrayList;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemsAdapter extends BaseAdapter {

	private final Context context;
	private final ArrayList<Item> items;

	public ItemsAdapter(Context context, ArrayList<Item> items) {
		this.context = context;
		this.items = items;
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public Item getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context)
					.inflate(R.layout.item_adapter, parent, false);

			holder = new ViewHolder();
			holder.txtName = (TextView) convertView.findViewById(R.id.txtName);
			holder.txtDescription = (TextView) convertView.findViewById(R.id.txtDescription);
			holder.txtCredits = (TextView) convertView.findViewById(R.id.txtCredits);
			holder.imgItem = (ImageView) convertView.findViewById(R.id.imgItem);
			holder.imgInfo = (ImageView) convertView.findViewById(R.id.imgInfo);

			convertView.setTag(holder);

		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		Item item = getItem(position);

		holder.txtName.setText(item.getName());
		holder.txtDescription.setText(item.getDescription());
		holder.txtCredits.setText(context.getString(R.string.credits_to) + item.getCredits());
		holder.imgItem.setImageDrawable(item.getImage());

		holder.imgInfo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (holder.txtDescription.getVisibility() == View.GONE) {
					holder.txtDescription.setAlpha(0f);
					holder.txtDescription.setVisibility(View.VISIBLE);
					holder.txtDescription.animate().alpha(1f).setDuration(500).setListener(null);

				} else if (holder.txtDescription.getVisibility() == View.VISIBLE) {
					holder.txtDescription.animate().alpha(0f).setDuration(500)
							.setListener(new AnimatorListener() {

								@Override
								public void onAnimationStart(Animator animation) {
								}

								@Override
								public void onAnimationRepeat(Animator animation) {
								}

								@Override
								public void onAnimationEnd(Animator animation) {
									holder.txtDescription.setVisibility(View.GONE);
								}

								@Override
								public void onAnimationCancel(Animator animation) {
								}
							});
				}
			}
		});

		return convertView;
	}

	private class ViewHolder {
		TextView txtName, txtCredits, txtDescription;
		ImageView imgItem, imgInfo;
	}

}
