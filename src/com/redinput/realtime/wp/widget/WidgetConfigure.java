package com.redinput.realtime.wp.widget;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.redinput.realtime.wp.R;

public class WidgetConfigure extends Activity implements OnClickListener, OnItemSelectedListener {

	private int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID, mCurrentImage = -1;

	private TextView lblOption;
	private Spinner spinImage, spinOption;
	private Button btnDiscard, btnSave;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.widget_config);

		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			mAppWidgetId = extras.getInt(AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);
		}
		setResult(RESULT_CANCELED);

		if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			Toast.makeText(this, "Something wrong with your widget", Toast.LENGTH_SHORT).show();
			finish();
		}

		onInit();
	}

	private void onInit() {
		lblOption = (TextView) findViewById(R.id.lbl_option);
		spinImage = (Spinner) findViewById(R.id.spin_image);
		spinOption = (Spinner) findViewById(R.id.spin_option);
		btnDiscard = (Button) findViewById(R.id.btn_discard_widget);
		btnSave = (Button) findViewById(R.id.btn_save_widget);

		spinImage.setOnItemSelectedListener(this);
		btnDiscard.setOnClickListener(this);
		btnSave.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.btn_discard_widget:
				finish();
				break;

			case R.id.btn_save_widget:
				Intent resultValue = new Intent();
				resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
				setResult(RESULT_OK, resultValue);

				SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
				Editor edit = sharedPref.edit();
				edit.putInt(mAppWidgetId + "-image", spinImage.getSelectedItemPosition());
				edit.putInt(mAppWidgetId + "-option", spinOption.getSelectedItemPosition());
				edit.apply();

				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
				WidgetProvider.buildWidget(this, appWidgetManager, mAppWidgetId);

				finish();
				break;
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position,
			long id) {
		if (mCurrentImage != position) {
			ArrayAdapter<String> spinnerArrayAdapter;
			switch (position) {
				case 0:
					spinnerArrayAdapter = new ArrayAdapter<String>(this,
							android.R.layout.simple_spinner_item, getResources().getStringArray(
									R.array.earth_options));
					spinnerArrayAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinOption.setAdapter(spinnerArrayAdapter);

					lblOption.setEnabled(true);
					spinOption.setEnabled(true);

					break;

				case 1:
					spinnerArrayAdapter = new ArrayAdapter<String>(this,
							android.R.layout.simple_spinner_item, new String[] { "" });
					spinnerArrayAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinOption.setAdapter(spinnerArrayAdapter);

					lblOption.setEnabled(false);
					spinOption.setEnabled(false);

					break;

				case 2:
					spinnerArrayAdapter = new ArrayAdapter<String>(this,
							android.R.layout.simple_spinner_item, getResources().getStringArray(
									R.array.sun_options));
					spinnerArrayAdapter
							.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
					spinOption.setAdapter(spinnerArrayAdapter);

					lblOption.setEnabled(true);
					spinOption.setEnabled(true);

					break;
			}

			mCurrentImage = position;
		}
	}

	@Override
	public void onNothingSelected(AdapterView<?> parentView) {
	}

}
