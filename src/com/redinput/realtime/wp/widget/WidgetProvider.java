package com.redinput.realtime.wp.widget;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

import com.redinput.realtime.wp.R;

public class WidgetProvider extends AppWidgetProvider {

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		final int N = appWidgetIds.length;
		for (int i = 0; i < N; i++) {
			buildWidget(context, appWidgetManager, appWidgetIds[i]);
		}
	}

	public static void buildWidget(Context context, AppWidgetManager appWidgetManager,
			int appWidgetId) {
		SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);

		if (sharedPref.contains(appWidgetId + "-image")) {
			long interval = Long.valueOf(sharedPref.getString("updates_interval", "3600000"));

			final AlarmManager alarmManager = (AlarmManager) context
					.getSystemService(Context.ALARM_SERVICE);

			Intent intent = new Intent(context, WidgetService.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
			PendingIntent mPendingService = PendingIntent.getService(context, appWidgetId, intent,
					PendingIntent.FLAG_CANCEL_CURRENT);

			alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
					SystemClock.elapsedRealtime() + interval, interval, mPendingService);
			context.startService(intent);
		}

		RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_initial);
		appWidgetManager.updateAppWidget(appWidgetId, views);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		final AlarmManager alarmManager = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		final int N = appWidgetIds.length;
		for (int i = 0; i < N; i++) {
			Intent intent = new Intent(context, WidgetService.class);
			intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetIds[i]);
			PendingIntent mPendingService = PendingIntent.getService(context, appWidgetIds[i],
					intent, PendingIntent.FLAG_CANCEL_CURRENT);

			alarmManager.cancel(mPendingService);
		}

		super.onDeleted(context, appWidgetIds);
	}

}
