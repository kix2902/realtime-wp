package com.redinput.realtime.wp.widget;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.view.Display;
import android.view.WindowManager;
import android.widget.RemoteViews;

import com.redinput.realtime.wp.R;
import com.redinput.realtime.wp.Utils;

public class WidgetService extends Service {

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		if (Utils.shouldDownload(this)) {
			new AsyncDownload(intent, this).execute();

			return START_STICKY;
		}
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	private class AsyncDownload extends AsyncTask<Void, Void, Bitmap> {
		private String url;
		private final int appWidgetId;
		private final Context context;

		public AsyncDownload(Intent intent, Context context) {
			this.context = context;

			appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
					AppWidgetManager.INVALID_APPWIDGET_ID);

			SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
			int image = sharedPref.getInt(appWidgetId + "-image", 0);
			int option = sharedPref.getInt(appWidgetId + "-option", 0);

			switch (image) {
				case 0:
					switch (option) {
						case 0:
							url = "http://static.die.net/earth/mercator/1600.jpg";
							break;

						case 1:
							url = "http://static.die.net/earth/peters/1600.jpg";
							break;

						case 2:
							url = "http://static.die.net/earth/rectangular/1600.jpg";
							break;
					}
					break;

				case 1:
					url = "http://static.die.net/moon/512.jpg";
					break;

				case 2:
					switch (option) {
						case 0:
							url = "http://soho.esac.esa.int/data/realtime/eit_171/1024/latest.jpg";
							break;

						case 1:
							url = "http://soho.esac.esa.int/data/realtime/eit_195/1024/latest.jpg";
							break;

						case 2:
							url = "http://soho.esac.esa.int/data/realtime/eit_284/1024/latest.jpg";
							break;

						case 3:
							url = "http://soho.esac.esa.int/data/realtime/eit_304/1024/latest.jpg";
							break;
					}
					break;
			}

		}

		@Override
		protected Bitmap doInBackground(Void... params) {
			WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
			Display display = wm.getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			int width = size.x;
			int height = size.y;

			try {
				return decodeSampledBitmapFromStream(width, height);

			} catch (MalformedURLException e) {
				e.printStackTrace();

			} catch (IOException e) {
				e.printStackTrace();

			} catch (URISyntaxException e) {
				e.printStackTrace();
			}

			return null;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			super.onPostExecute(result);

			if (result != null) {
				RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);
				views.setImageViewBitmap(R.id.imgWidget, result);

				AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
				appWidgetManager.updateAppWidget(appWidgetId, views);
			}

			stopSelf();
		}

		private int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {
				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		private Bitmap decodeSampledBitmapFromStream(int reqWidth, int reqHeight)
				throws URISyntaxException, IOException {

			AndroidHttpClient client = AndroidHttpClient.newInstance("Realtime WP 2.0/Android");
			HttpGet get = new HttpGet();
			get.setURI(new URI(url));

			HttpResponse response = client.execute(get);
			InputStream is = response.getEntity().getContent();
			BufferedInputStream bis = new BufferedInputStream(is);

			final BitmapFactory.Options options = new BitmapFactory.Options();
			options.inJustDecodeBounds = true;
			Rect rect = new Rect(-1, -1, -1, -1);
			BitmapFactory.decodeStream(bis, rect, options);

			// Calculate inSampleSize
			options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

			// Decode bitmap with inSampleSize set
			options.inJustDecodeBounds = false;

			response = client.execute(get);
			is = response.getEntity().getContent();
			bis = new BufferedInputStream(is);

			Bitmap bmp = BitmapFactory.decodeStream(bis, rect, options);

			bis.close();
			is.close();
			client.close();

			return bmp;
		}

	}

}
